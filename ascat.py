# run ascat r library over python3

# python standard libaries
import csv
import os
import re
import sys
# scipy libraries
import pandas as pd
# R over rpy2 libararies
import rpy2.robjects as robjects
#robjects.r("library(copynumber)")  # pcf
robjects.r("library(RColorBrewer)")
robjects.r("library(ASCAT)")

# pwn
from myascat.ascattrafo import txtsegdiff, txtmajorminor2txtseg, txtmajorminor2txtfasta
from myascat.pcf import plotallele, plotgamma

####################
# internal methods #
####################
def getcel(s_pathmain, s_ifileprj, s_ipathprj, s_ifilehapmap, s_ipathhapmap):
    """
    description:
      *internal method*,
      get path to project and hapmap cel files.
      put output into celhapmap0k.txt and celprj0k.txt files

    input:
      - ldts_sample: link to CEL files and XX XY information
      - s_pathmain: main input output path
      - s_ifileprj: filename of file with contains a listing of the project CEL files
      - s_ipathprj: path to project cell file folder; note: symbolic links will not work
      - s_ifilehapmap: filename of file with contains a listing of the hapmap reference CEL files
      - s_ipathhapmap: path to hapmap reference cell file folder; note: symbolic links will not work

    output:
      - s_pathmain/01getcel/celprjhapmap0k.txt
      - s_pathmain/01getcel/celprj0k.txt.
    """
    # cat hapmap and prj cel file colection for birdseed algorithm
    # initialize output
    b_0k = False
    s_ofile = None

    # handle input
    s_ifilehapmap = s_ipathhapmap + s_ifilehapmap
    s_ifileprj = s_ipathprj + s_ifileprj
    s_opath = s_pathmain + "01getcel/"
    os.mkdir(s_opath)

    # output file
    print("\nProcessing getcel hapmap and prj cel file location ...")
    # outputfile hapmap and prj
    s_ofile = s_opath + "celprjhapmap0k.txt"
    with open(s_ofile, "w") as f_output:
        # header
        f_output.write("cel_files\n")
        # prj cel files
        with open(s_ifileprj, "r") as f_iprj:
            for s_entry in f_iprj:
                if re.match(".*cel_files$", s_entry):
                    f_output.write(s_entry)
                else:
                    f_output.write(s_ipathprj + s_entry)
        f_iprj.close()
        # hapmap cel files
        with open(s_ifilehapmap, "r") as f_ihapmap:
            for s_entry in f_ihapmap:
                f_output.write(s_ipathhapmap + s_entry)
        f_ihapmap.close()
    f_output.close()
    # check for outout file
    b_0k = os.path.isfile(s_ofile)
    b_0k = False

    # outputfile prj
    s_ofile = s_opath + "celprj0k.txt"
    with open(s_ofile, "w") as f_output:
        # header
        f_output.write("cel_files\n")
        # prj cel files
        with open(s_ifileprj, "r") as f_iprj:
            for s_entry in f_iprj:
                if re.match(".*cel_files$", s_entry):
                    f_output.write(s_entry)
                else:
                    f_output.write(s_ipathprj + s_entry)
        f_iprj.close()
    f_output.close()
    # check for outout file
    b_0k = os.path.isfile(s_ofile)
    print("0K:", b_0k)
    return(b_0k)


def aptprobsetgenotype(s_pathmain, s_ipathlib, s_ipath="01getcel/"):
    """
    description:
      *internal method*,
      execute affymetrix apt-probeset-genotype shell command.

    input:
      - s_pathmain: main input output path
      - s_ipath: input sub path
      - s_ipathlib: path to affy apt-probeset-genotype, affy apt-probeset-summarize,
            pencnv normalize_affy_geno_cluster.pl and ascat sourcecode related library

    output:
      - s_pathmain/02aptprobsetgenotype/apt-probeset-genotype.log
      - s_pathmain/02aptprobsetgenotype/birdseed.calls.txt
      - s_pathmain/02aptprobsetgenotype/birdseed.confidences.txt
      - s_pathmain/02aptprobsetgenotype/birdseed.report.txt
    """

    # initialize outout
    s_cmd = None

    # handle input
    s_ipath = s_pathmain + s_ipath
    s_opath = s_pathmain + "02aptprobsetgenotype/"
    os.mkdir(s_opath)

    # affymetrix power tool apt-probset-genotype
    print("\nProcessing apt-probset-genotype ...")
    s_cmd = "apt-probeset-genotype" +\
            " -c " + s_ipathlib + "GenomeWideSNP_6.cdf" +\
            " -a birdseed --read-models-birdseed " + s_ipathlib + "GenomeWideSNP_6.birdseed.models" +\
            " --special-snps " + s_ipathlib + "GenomeWideSNP_6.specialSNPs" +\
            " --out-dir " + s_opath +\
            " --cel-files " + s_ipath + "celprjhapmap0k.txt" +\
            " --use-disk true " +\
            " --chip-type GenomeWideEx_6 --chip-type GenomeWideSNP_6"
    i_run = os.system(s_cmd)
    print("cmd:", s_cmd, i_run)

    # check for outout file
    b_0k = os.path.isfile(s_opath + "apt-probeset-genotype.log") and os.path.isfile(s_opath + "birdseed.calls.txt") and os.path.isfile(s_opath + "birdseed.confidences.txt") and os.path.isfile(s_opath + "birdseed.report.txt")
    return(s_cmd)


def aptprobesetsummarizes(s_pathmain, s_ipathlib, s_icel="01getcel/celprj0k.txt", s_ipath="02aptprobsetgenotype/"):
    """
    description:
      *internal method*,
      execute affymetrix apt-probeset-summarize shell command.

    input:
      - s_pathmain: main input output path
      - s_icel: path to cell file listing file
      - s_ipath: input sub path

    output:
      - s_pathmain/03aptprobesetsummarizes/apt-probeset-summarize.log
      - s_pathmain/03aptprobesetsummarizes/quant-norm.pm-only.med-polish.expr.report.txt
      - s_pathmain/03aptprobesetsummarizes/quant-norm.pm-only.med-polish.expr.summary.txt
    """

    # initialize outout
    s_cmd = None

    # handle input
    s_icel = s_pathmain + s_icel
    s_ipath = s_pathmain + s_ipath
    s_opath = s_pathmain + "03aptprobesetsummarizes/"
    os.mkdir(s_opath)

    # set work directory
    s_pwd = os.getcwd()
    os.chdir(s_ipath)

    # affymetrix power tool apt-probeset-summarizes
    print("\nProcessing apt-probeset-summarize ...")
    s_cmd = "apt-probeset-summarize" +\
            " --cdf-file " + s_ipathlib + "GenomeWideSNP_6.cdf" +\
            " --analysis quant-norm.sketch=50000,pm-only,med-polish,expr.genotype=true" +\
            " --target-sketch " + s_ipathlib + "hapmap.quant-norm.normalization-target.txt" +\
            " --out-dir " + s_opath +\
            " --cel-files " + s_icel +\
            " --chip-type GenomeWideEx_6 --chip-type GenomeWideSNP_6"
    i_run = os.system(s_cmd)
    print("cmd:", s_cmd, i_run)

    # check for outout file
    b_0k = os.path.isfile(s_opath + "apt-probeset-summarize.log") and os.path.isfile(s_opath + "quant-norm.pm-only.med-polish.expr.report.txt") and os.path.isfile(s_opath + "quant-norm.pm-only.med-polish.expr.summary.txt")
    return(b_0k)

    # set work directory back
    os.chdir(s_pwd)
    return(b_0k)


def penncnvnormalizeaffygenocluster(s_pathmain, s_ipathlib, s_ipath="03aptprobesetsummarizes/"):
    # bue 20160218: pencnv geno hg19 hapmap.cluster replaced by ascat hg19 gw6.genotype cluster
    """
    description:
      *internal method*,
      execute pencnv normalize_affy_geno_cluster.pl perl script.
      put output into lrr_baf.txt file

    input:
      - s_pathmain: main input output path
      - s_ipathlib: path to affy apt-probeset-genotype, affy apt-probeset-summarize,
            pencnv normalize_affy_geno_cluster.pl and ascat sourcecode related library
      - s_ipath: input subpath

    output:
      - s_pathmain/04penncnvnormalizeaffygenocluster/lrr_baf.txt
    """

    # handle input
    s_ipath = s_pathmain + s_ipath
    s_opath = s_pathmain + "04penncnvnormalizeaffygenocluster/"
    os.mkdir(s_opath)

    # PennCNV perl script
    print("\nProcessing normalize_affy_geno_cluster.pl ...")
    s_cmd = s_ipathlib + "normalize_affy_geno_cluster.pl " +\
            s_ipathlib + "gw6.genocluster " +\
            s_ipath + "quant-norm.pm-only.med-polish.expr.summary.txt " +\
            "-locfile " + s_ipathlib + "affygw6.hg19.pfb " +\
            "-out " + s_opath + "lrr_baf.txt"


    i_run = os.system(s_cmd)
    print("cmd:", s_cmd, i_run)

    # check for outout file
    b_0k = os.path.isfile(s_opath + "lrr_baf.txt")
    return(b_0k)


def lrrbaf2logrbaf(ldts_sample, s_pathmain, s_ipathlib, s_ipath="04penncnvnormalizeaffygenocluster/", s_regex=""):
    """
    description:
      *internal method*,
      manipulate lrr_baf.txt file
      get illumina and ascat compatible BAF.txt and and LogR.txt files per sample.

    input:
      - ldts_sample: link to CEL files and XX XY information
      - s_pathmain: main input output path
      - s_ifileprj: filename of file with contains a listing of the project CEL files
      - s_ipathprj: path to project cell file folder; note: symbolic links will not work
      - s_ifilehapmap: filename of file with contains a listing of the hapmap reference CEL files
      - s_ipathhapmap: path to hapmap reference cell file folder; note: symbolic links will not work
      - s_ipathlib: path to affy apt-probeset-genotype, affy apt-probeset-summarize,
            pencnv normalize_affy_geno_cluster.pl and ascat sourcecode related library
      - s_regex: regular expressson sample name filter

    output:
      - s_pathmain/05lrrbaf2logrbaf/*BAF.txt
      - s_pathmain/05lrrbaf2logrbaf/*LogR.txt
    """

    # initialize output
    b_0K = False

    # handle input
    s_ipath = s_pathmain + s_ipath
    s_opath = s_pathmain + "05lrrbaf2logrbaf/"
    os.mkdir(s_opath)

    # read SNPpos file
    print("\nRead SNPpos.txt ...")
    df_snppos = pd.read_csv(s_ipathlib + "SNPpos.txt", sep='\t', index_col=0)
    df_snppos.columns = ["Chromosome","PhysicalPosition"]
    #print(df_snppos)
    print("0K")

    # read LogR BAF file
    print("\nRead lrr_baf.txt ...")
    df_lrrbaf = pd.read_csv(s_ipath + "lrr_baf.txt", sep='\t', index_col="Name")
    # kick out locuses not in snppos
    df_logrbaf = pd.concat([df_snppos, df_lrrbaf], axis=1, join="inner")
    #print(df_logrbaf)
    print("0K")

    # initialize 0k samlpe set
    es_0ksample = set()

    # for each sample
    for dts_sample in ldts_sample:

        # regex processing filter
        o_regex = re.search(s_regex, dts_sample["mutant"][0])
        if (o_regex == None):
            print("\nSkipping lrrbaf2logrbaf {} ...".format(dts_sample))
        else:
            print("\nProcessing lrrbaf2logrbaf {} ...".format(dts_sample))

            # wildtype
            try:
                ts_wildtype = dts_sample["wildtype"]
            except KeyError:
                ts_wildtype = None  # (None,None)
            # mutant
            try:
                ts_mutant = dts_sample["mutant"]
            except KeyError:
                sys.exit("Mutant sample missing, and only wid type sample can be missing.")

            # wildtype data
            if (ts_wildtype != None) and not (ts_wildtype[1] in es_0ksample) :
                print("wildtype ...")
                # get wildtype baf data in shape
                s_bafwildtype = ts_wildtype[1] + ".B Allele Freq"
                s_bafwildtype_new = ts_wildtype[0]
                s_bafwildtype_file = ts_wildtype[0] + "_BAF" + ".txt"
                df_bafwildtype = df_logrbaf[["Chromosome","PhysicalPosition",s_bafwildtype]].copy()
                df_bafwildtype.loc[df_bafwildtype[s_bafwildtype] == 2, s_bafwildtype] = "NA"
                # relable column
                df_bafwildtype.columns = ["Chromosome","PhysicalPosition", s_bafwildtype_new]  #bue 20160614: s_bafwildtype
                # write to file
                df_bafwildtype.to_csv(s_opath + s_bafwildtype_file, sep='\t', index_label="SNP")

                # get wildtype logr data in shape
                s_logrwildtype = ts_wildtype[1] + ".Log R Ratio"
                s_logrwildtype_new = ts_wildtype[0]
                s_logrwildtype_file = ts_wildtype[0] + "_LogR" + ".txt"
                df_logrwildtype = df_logrbaf[["Chromosome","PhysicalPosition",s_logrwildtype]].copy()
                # copy number only
                df_logrwildtype.loc[df_logrwildtype.index.str.contains('CN'), s_logrwildtype_new]\
                    = df_logrwildtype.loc[df_logrwildtype.index.str.contains('CN'), s_logrwildtype]\
                    - df_logrwildtype.loc[df_logrwildtype.index.str.contains('CN'), s_logrwildtype].mean(0)
                # snp only
                df_logrwildtype.loc[df_logrwildtype.index.str.contains('SNP'), s_logrwildtype_new]\
                    = df_logrwildtype.loc[df_logrwildtype.index.str.contains('SNP'), s_logrwildtype]\
                    - df_logrwildtype.loc[df_logrwildtype.index.str.contains('SNP'), s_logrwildtype].mean(0)
                # relable column
                # write to file
                df_logrwildtype[["Chromosome","PhysicalPosition",s_logrwildtype_new]].to_csv(s_opath + s_logrwildtype_file, sep='\t', index_label="SNP")

                # ok
                es_0ksample.add(ts_wildtype[1])

            if not (ts_mutant[1] in es_0ksample):
                print("mutant ...")
                # get mutant baf data in shape
                s_bafmutant = ts_mutant[1] + ".B Allele Freq"
                s_bafmutant_new = ts_mutant[0]
                s_bafmutant_file = ts_mutant[0] + "_BAF" + ".txt"
                df_bafmutant = df_logrbaf[["Chromosome","PhysicalPosition",s_bafmutant]].copy()
                df_bafmutant.loc[df_bafmutant[s_bafmutant] == 2, s_bafmutant] = "NA"
                # relable column
                df_bafmutant.columns = ["Chromosome","PhysicalPosition", s_bafmutant_new]
                # write to file
                df_bafmutant.to_csv(s_opath + s_bafmutant_file, sep='\t', index_label="SNP")

                # get mutant logr data in shape
                s_logrmutant = ts_mutant[1] + ".Log R Ratio"
                s_logrmutant_new = ts_mutant[0]
                s_logrmutant_file = ts_mutant[0] + "_LogR" + ".txt"
                df_logrmutant = df_logrbaf[["Chromosome","PhysicalPosition",s_logrmutant]].copy()
                # copy number only
                df_logrmutant.loc[df_logrmutant.index.str.contains('CN'), s_logrmutant_new]\
                    = df_logrmutant.loc[df_logrmutant.index.str.contains('CN'), s_logrmutant]\
                    - df_logrmutant.loc[df_logrmutant.index.str.contains('CN'), s_logrmutant].mean(0)
                # snp only
                df_logrmutant.loc[df_logrmutant.index.str.contains('SNP'), s_logrmutant_new]\
                    = df_logrmutant.loc[df_logrmutant.index.str.contains('SNP'), s_logrmutant]\
                    - df_logrmutant.loc[df_logrmutant.index.str.contains('SNP'), s_logrmutant].mean(0)
                # relable column
                # write to file
                df_logrmutant[["Chromosome","PhysicalPosition",s_logrmutant_new]].to_csv(s_opath + s_logrmutant_file, sep='\t', index_label="SNP")

                # ok
                es_0ksample.add(ts_mutant[1])

        # sample processed
        print("0K\n")

    # output
    b_0k = True
    return("b_0K")


def txtpcf2txtseg(s_ipath, s_ifile, s_ipathlib, s_isnpos, s_opath):
    """
    *internal methode*:
    morph pcf segmented ascat output into igv genome browswer readable seq format

    input:
      - s_ipath: main input path
      - s_ifile: pcf track input file
      - s_ipathlib: path to affy apt-probeset-genotype, affy apt-probeset-summarize,
            pencnv normalize_affy_geno_cluster.pl and ascat sourcecode related library
      - s_isnpos: SNP position file name in s_ipathlib
      - s_opath: main output path

    output:
      - lt_pcf list of (s_ofile, o_chromosome, i_start, i_stop, i_mark, i_value) tuples
    """
    print("\nProcessing {} ...".format({s_ifile}))
    # read SNPpos file
    print("\nRead {} ...".format({s_ipathlib+s_isnpos}))
    df_snppos = pd.read_csv(s_ipathlib + s_isnpos, sep='\t', index_col=0)
    df_snppos.columns = ["chromosome","position"]
    #print(df_snppos)
    print("0K")

    # read pcf file
    print("\nRead {} ...".format({s_ipath+s_ifile}))
    df_pcf = pd.read_csv(s_ipath + s_ifile, sep='\t', index_col=0, header=None)
    df_pcf.columns = ["value"]
    #print(df_pcf)
    print("0K")

    # inner join snppos pcf
    df_snppospcf = pd.concat([df_snppos, df_pcf], axis=1, join="inner")
    #print(df_snppospcf)
    df_snppospcf = df_snppospcf.sort_index(axis=0, level="chromosome_value")
    # sort by chromosome and position
    df_snppospcf.sort_values(by=['chromosome','position'], axis="index", ascending=[True,True], inplace=True)

    # iterate through table. fuse same consecutive segments.
    print("\nFuse same consecutive segments and write file...")
    lt_pcf = [("track","chromosome","begin","end", "marks", "value"),]
    s_ofile = s_ifile.replace(".txt","")
    o_chromosome = None
    i_position = None
    i_mark = None
    i_value = None
    for t_entry in df_snppospcf.itertuples():
        o_chr = t_entry.chromosome
        i_pos = t_entry.position
        i_val = t_entry.value
        if (o_chr != o_chromosome) or (i_val != i_value):
            if (o_chromosome != None):
                t_pcf = (s_ofile, o_chromosome, i_start, i_stop, i_mark, i_value)
                lt_pcf.append(t_pcf)
            o_chromosome = o_chr
            i_start = i_pos
            i_stop = i_pos
            i_mark = 1
            i_value = i_val
        else:
            o_chromosome = o_chr
            i_stop = i_pos
            i_mark += 1
            i_value = i_val
    # add last segment
    if (o_chromosome != None):
        t_pcf = (s_ofile, o_chromosome, i_start, i_stop, i_mark, i_value)
        lt_pcf.append(t_pcf)
    # output
    #print(lt_pcf)
    s_ofile = s_ofile + ".seg"
    s_out = s_opath + s_ofile
    #print(s_out)
    with open(s_out, 'w', newline='') as f:
        writer = csv.writer(f, delimiter='\t')
        writer.writerows(lt_pcf)
    print("0K")
    print("0K")
    return(lt_pcf)



################
# main methods #
################
def cel2logrbaf(ldts_sample, s_pathmain, s_ifileprj, s_ipathprj, s_ifilehapmap, s_ipathhapmap, s_ipathlib, s_regex="", b_jump=True):
    """
    description:
      if b_jump is False, execute internal methodes:
        - getcel
        - aptprobsetgenotype (time consuming)
        - aptprobesetsummarizes (time consuming)
        - penncnvnormalizeaffygenocluster
        - lrrbaf2logrbaf

      if b_jump is True (default), execute only internal methodes:
        - penncnvnormalizeaffygenocluster
        - lrrbaf2logrbaf

    input:
      - ldts_sample: link to CEL files and XX XY information
      - s_pathmain: main input output path
      - s_ifileprj: filename of file with contains a listing of the project CEL files
      - s_ipathprj: path to project cell file folder; note: symbolic links will not work
      - s_ifilehapmap: filename of file with contains a listing of the hapmap reference CEL files
      - s_ipathhapmap: path to hapmap reference cell file folder; note: symbolic links will not work
      - s_ipathlib: path to affy apt-probeset-genotype, affy apt-probeset-summarize,
            pencnv normalize_affy_geno_cluster.pl and ascat sourcecode related library
      - s_regex: regular expressson sample name filter

    output:
      - s_pathmain/05lrrbaf2logrbaf/*BAF.txt
      - s_pathmain/05lrrbaf2logrbaf/*LogR.txt
    """

    # initialize output
    b_0K = False
    if not(os.path.isdir(s_pathmain)):
        os.mkdir(s_pathmain)

    b_getcel = getcel(s_pathmain=s_pathmain, s_ifileprj=s_ifileprj, s_ipathprj=s_ipathprj, s_ifilehapmap=s_ifilehapmap, s_ipathhapmap=s_ipathhapmap)
    if not(b_jump):
        b_aptprobsetgenotype = aptprobsetgenotype(s_pathmain=s_pathmain, s_ipathlib=s_ipathlib)
        b_aptprobesetsummarizes = aptprobesetsummarizes(s_pathmain=s_pathmain, s_ipathlib=s_ipathlib)
        b_penncnvnormalizeaffygenocluster = penncnvnormalizeaffygenocluster(s_pathmain=s_pathmain, s_ipathlib=s_ipathlib)
    else:
        b_aptprobsetgenotype = True
        b_aptprobesetsummarizes = True
        b_penncnvnormalizeaffygenocluster = True
    # always
    b_logrbaf = lrrbaf2logrbaf(ldts_sample=ldts_sample, s_pathmain=s_pathmain, s_ipathlib=s_ipathlib, s_regex=s_regex)
    # output
    b_0k = b_getcel and b_aptprobsetgenotype and b_aptprobesetsummarizes and b_penncnvnormalizeaffygenocluster and b_logrbaf
    return("b_0K")


def logrbaf2plotallele(ldts_sample, s_pathmain, s_ipath="05lrrbaf2logrbaf/", i_pcfgamma=40, s_regex=""):
    """
    input:
    output:
      - s_pathmain/06pcf40/*
    """
    # run ascat
    # initialize output
    ls_out = []

    # set work directory
    s_ipath = s_pathmain + s_ipath

    # for each sample
    for dts_sample in ldts_sample:

        # regex processing filter
        o_regex = re.search(s_regex, dts_sample["mutant"][0])
        if (o_regex == None):
            print("\nSkipping logrbaf2plotallele {} ...".format(dts_sample))
        else:
            print("\nProcessing logrbaf2plotallele {} ...".format(dts_sample))

            # get mutant sample
            try:
                ts_mutant = dts_sample["mutant"]
            except KeyError:
                sys.exit("Mutant sample {} missing, and only wildtype sample can be missing.".format(ts_mutant[0]))

            # get filenames and i_pcfgamma
            s_baf = ts_mutant[0] + "_BAF.txt"
            s_logr = ts_mutant[0] + "_LogR.txt"
            try:
                i_gamma = ts_mutant[5]
            except IndexError:
                i_gamma = i_pcfgamma  # pcf default 40; ascat default 25

            # call function
            ls_sample = plotallele(s_logr=s_logr, s_baf=s_baf, s_pathmain=s_pathmain, s_ipath=s_ipath, i_gamma=i_gamma, b_verbose=True)
            ls_out.extend(ls_sample)

    # output
    return(ls_out)


def logrbaf2plotgamma(ldts_sample, s_pathmain, s_ipath="05lrrbaf2logrbaf/", li_gammarange=[10,100], s_regex=""):
    """
    input:
    output:
      - s_pathmain/06pcf/*
    """
    # run ascat
    # initialize output
    ls_out = []

    # set work directory
    s_ipath = s_pathmain + s_ipath

    # for each sample
    for dts_sample in ldts_sample:

        # regex processing filter
        o_regex = re.search(s_regex, dts_sample["mutant"][0])
        if (o_regex == None):
            print("\nSkipping logrbaf2plotallele {} ...".format(dts_sample))
        else:
            print("\nProcessing logrbaf2plotallele {} ...".format(dts_sample))

            # get mutant sample
            try:
                ts_mutant = dts_sample["mutant"]
            except KeyError:
                sys.exit("Mutant sample {} missing, and only wildtype sample can be missing.".format(ts_mutant[0]))

            # get filename
            s_logr = ts_mutant[0] + "_LogR.txt"

            # call function
            ls_sample = plotgamma(s_trackfile=s_logr, s_pathmain=s_pathmain, s_ipath=s_ipath, li_gammarange=li_gammarange)
            ls_out.extend(ls_sample)

    # output
    return(ls_out)


def logrbaf2ascat(ldts_sample, s_pathmain, s_ipathlib, s_ipath="05lrrbaf2logrbaf/", s_ipathsex="02aptprobsetgenotype/", i_pcfgamma=40, b_gccorrect=True, s_regex=""):
    """
    description:
      - run ascat on *BAF.txt and *LogR.txt fiels

    input:
      - ldts_sample: link to CEL files and XX XY information
      - s_pathmain: main input output path
      - s_ipath: input subpath
      - s_ipathsex: input computed XY XX subpath
      - s_regex: regular expressson sample name filter
      - b_gccorrect: GC correction boolean. for details see ascat manual. default True.

    structur ldts_sample:

      full length tuple structure is:
      wildtype:("s_label","s_file","s_sex")
      mutant:("s_label","s_file","s_sex",f_aberrant,f_ploidy,i_pcfgamma)

      possible s_sex values are: None (check computed sex), "NA", "XY", "XX"
      default f_aberrant value is: None (retreive by algorithm)
      default f_ploidy value is: None (retreive by algorithm)
      default i_pcfgamma value is: 40 (default pcf; default ascat 25)

      example code:
      ldts_sample = []
      # one
      dts_entry = {}
      dts_entry.update({"mutant":("LABEL","FILE.CEL","XX")})
      ldts_sample.append(dts_entry)
      # two
      dts_entry = {}
      dts_entry.update({"mutant":("LABEL","FILE.CEL","XX",None,None,25)})
      ldts_sample.append(dts_entry)
      # three
      dts_entry = {}
      dts_entry.update({"wildtype":("LABELwt","FILEwt.CEL","XX")})
      dts_entry.update({"mutant":("LABELmut","FILE.CEL","XX")})
      ldts_sample.append(dts_entry)
      # four
      dts_entry = {}
      dts_entry.update({"wildtype":("LABELwt","FILE.CEL","XX")})
      dts_entry.update({"mutant":("LABELmut","FILE.CEL","XX",None,None,25)})
      ldts_sample.append(dts_entry)

    output:
      - s_pathmain/06ascatNN/*.germline.png
      - s_pathmain/06ascatNN/*.tumour.png
      - s_pathmain/06ascatNN/*.ASPCF.png
      - s_pathmain/06ascatNN/*.BAF.PCFed.txt
      - s_pathmain/06ascatNN/*.LogR.PCFed.txt
      - s_pathmain/06ascatNN/*.sunrise.png
      - s_pathmain/06ascatNN/*.aberrationreliability.png
      - s_pathmain/06ascatNN/*.rawprofile.png
      - s_pathmain/06ascatNN/*.ASCATprofile.png
    """

    # run ascat
    # initialize output
    b_0K = False

    # set work directory
    s_pwd = os.getcwd()
    s_opath = s_pathmain + "06ascat" + str(i_pcfgamma) + "/"
    s_ipath = s_pathmain + s_ipath
    if not(os.path.isdir(s_opath)):
        os.mkdir(s_opath)
    os.chdir(s_opath)

    # get output subpaths
    s_opathraw = s_opath + "raw/"
    s_opathpcf = s_opath + "pcf_segmentation/"
    s_opathploidyaberration = s_opath + "ascat_ploidyaberration/"
    s_opathminormajor = s_opath + "ascat_minormajor/"
    s_opathascat =  s_opath + "ascat_profile/"

    # handle sex
    # read gender result
    try:
        df_sex = pd.read_csv(s_pathmain + s_ipathsex + "birdseed.report.txt", sep="\t", index_col="cel_files", comment="#")
        df_sex = df_sex[["computed_gender"]].copy()
        df_sex.loc[df_sex["computed_gender"] == "female", "computed_sex"] = "XX"
        df_sex.loc[df_sex["computed_gender"] == "male", "computed_sex"] = "XY"
        df_sex.loc[df_sex["computed_gender"] == "unknown", "computed_sex"] = "NA"
    except OSError:
        df_sex = None

    # for each sample
    for dts_sample in ldts_sample:

        # regex processing filter
        o_regex = re.search(s_regex, dts_sample["mutant"][0])
        if (o_regex == None):
            print("\nSkipping logrbaf2ascat {} ...".format(dts_sample))
        else:
            print("\nProcessing logrbaf2ascat {} ...".format(dts_sample))

            # wildtype
            try:
                ts_wildtype = dts_sample["wildtype"]
                s_wildtypesex = ts_wildtype[2]
                if (s_wildtypesex == "XX") or (s_wildtypesex == "XY"):
                    print("+ wildtype real sex:", s_wildtypesex)
                elif (s_wildtypesex == None):
                    s_wildtypesex = str(df_sex[df_sex.index == ts_wildtype[1]][["computed_sex"]])
                    print("+ wildtype cyber sex:", s_wildtypesex)
                else:
                    sys.exit("Wildtype sample {} unknowen sex type {}.".format(ts_wildtype[0], s_wildtypesex) +\
                             " Knowen are XX (female), XY (male), None (computed sex). If unsure choose XX")
            except KeyError:
                ts_wildtype = None

            # mutant
            try:
                ts_mutant = dts_sample["mutant"]
                # sex
                s_mutantsex = ts_mutant[2]
                if (s_mutantsex == "XX") or (s_mutantsex == "XY"):
                    print("+ mutant real sex:", s_mutantsex)
                elif (s_mutantsex == None):
                    s_mutantsex = str(df_sex[df_sex.index == ts_mutant[1]][["computed_sex"]])
                    print("+ mutant cyber sex:", s_mutantsex)
                else:
                    sys.exit("Mutant sample {} unknowen sex type {}.".format(ts_mutant[0], s_mutantsex) +\
                             " Knowen are XX (female), XY (male), None (computed sex). If unsure choose XX")

                # f_aberrant, f_ploidy, i_pcfgamma form mutant input
                try:
                    i_aspcfgamma = ts_mutant[5]
                    f_aberrant = ts_mutant[3]
                    f_ploidy = ts_mutant[4]
                except IndexError:
                    i_aspcfgamma = i_pcfgamma
                    f_aberrant = None
                    f_ploidy = None
                # check f_aberrant f_ploidy input
                if (f_aberrant == None) or (f_ploidy == None):
                    if not((f_aberrant == None) and (f_ploidy == None)):
                        sys.exit("f_aberrant {} and f_ploidy {} can only be set both or none.".format(f_aberrant, f_ploidy))
            except KeyError:
                sys.exit("Mutant sample {} missing, and only wildtype sample can be missing.".format(ts_mutant[0]))

            # get command
            if (ts_wildtype == None):
                # without matched wildtype
                s_command = "ascat.bc <- ascat.loadData('"+s_ipath+ts_mutant[0]+"_LogR.txt', '"+s_ipath+ts_mutant[0]+"_BAF.txt', chrs=c(1:22, 'X'), gender='" + s_mutantsex +"')"
            else:
                # with matched wildtype
                s_command = "ascat.bc <- ascat.loadData('"+s_ipath+ts_mutant[0]+"_LogR.txt', '"+s_ipath+ts_mutant[0]+"_BAF.txt', '"+s_ipath+ts_wildtype[0]+"_LogR.txt', '"+s_ipath+ts_wildtype[0]+"_BAF.txt', chrs=c(1:22, 'X'), gender=c('" + s_mutantsex + "','" + s_wildtypesex + "'))"
            # run command
            # load data
            print("ASCAT load data: {} ...".format(s_command))
            robjects.r(s_command)

            # gc correction
            if (b_gccorrect):
                # bue 20160221: snp names have to be affymetrix snp6 chip compatibe. Otheriwse the code will brake with:
                # Error in apply(corr_tot, 1, function(x) sum(abs(x * length_tot))/sum(length_tot)) : dim(X) must have a positive length

                s_command = "ascat.bc <- ascat.GCcorrect(ascat.bc, '" + s_ipathlib+"GC_AffySNP6_102015.txt')"
                print("ASCAT gc correction: {} ...".format(s_command))
                robjects.r(s_command)

            # ascat basics
            # rawdata
            s_command = "ascat.plotRawData(ascat.bc)"
            print("ASCAT plot rawa data: {} ...".format(s_command))
            robjects.r(s_command)
            os.chdir(s_opath)

            # segmentation
            if (ts_wildtype == None):
                # without matched wildtype
                s_command01 = "gg <- ascat.predictGermlineGenotypes(ascat.bc, platform = 'AffySNP6')"
                s_command02 = "ascat.bc <- ascat.aspcf(ascat.bc, ascat.gg=gg, penalty="+str(i_aspcfgamma)+")"
                robjects.r(s_command01)
            else:
                # with matched wildtype
                s_command01 = "matched wildtype"
                s_command02 = "ascat.bc <- ascat.aspcf(ascat.bc, penalty="+str(i_aspcfgamma)+")"
            s_command03 = "ascat.plotSegmentedData(ascat.bc)"
            print("ASCAT PCF segmentation:\n{}\n{}\n{} ...".format(s_command01, s_command02, s_command03))
            robjects.r(s_command02)
            robjects.r(s_command03)
            os.chdir(s_opath)

            # ascat main function
            # ploydy manual setting before aberrant manual setting before non manual setting
            if (f_ploidy != None) and (f_aberrant != None):
                # forced ploydy
                s_command = "ascat.output <- ascat.runAscat(ascat.bc, rho_manual=" + str(f_aberrant) +", psi_manual=" + str(f_ploidy) + ", circos='circos.ca')"
            else:
                # tissue or blood cells
                s_command = "ascat.output <- ascat.runAscat(ascat.bc, circos='circos.ca')"
            print("ASCAT basics: {} ...\n".format(s_command))
            robjects.r(s_command)

            # save ASCAT results
            print("ASCAT R result ...")
            # segments
            s_command = "write.table(ascat.output$segments, file=paste('" + ts_mutant[0] + "','.segments.txt',sep=''), sep='\t', quote=F, row.names=F)"
            robjects.r(s_command)
            # aberrant cell fraction
            s_command = "write.table(ascat.output$aberrantcellfraction, file=paste('" +  ts_mutant[0] + "','.acf.txt',sep=''), sep='\t', quote=F, row.names=F)"
            robjects.r(s_command)
            # ploidy
            s_command = "write.table(ascat.output$ploidy, file=paste('" + ts_mutant[0] + "','.ploidy.txt',sep=''), sep='\t', quote=F, row.names=F)"
            robjects.r(s_command)
            # save R image
            s_command = "save.image(paste('" + ts_mutant[0] + "','.RData',sep=''))"
            robjects.r(s_command)

            # move raw plots
            if not(os.path.isdir(s_opathraw)):
                os.mkdir(s_opathraw)
            os.system("mv *.tumour.png " + s_opathraw)
            os.system("mv *.germline.png " + s_opathraw)
            os.system("mv tumorSep*.png " + s_opathraw)

            # move segementation plots
            if not(os.path.isdir(s_opathpcf)):
                os.mkdir(s_opathpcf)
            os.system("mv *.ASPCF.png " + s_opathpcf)

            # move ascat plots ascat_ploidyaberration
            if not(os.path.isdir(s_opathploidyaberration)):
                os.mkdir(s_opathploidyaberration)
            os.system("mv *.aberrationreliability.png " + s_opathploidyaberration)
            os.system("mv *.sunrise.png " + s_opathploidyaberration)

            # move ascat plot ascat_minormajor
            if not(os.path.isdir(s_opathminormajor)):
                os.mkdir(s_opathminormajor)
            os.system("mv *.rawprofile.png " + s_opathminormajor)

            # move ascat plot ascat_profile
            if not(os.path.isdir(s_opathascat)):
                os.mkdir(s_opathascat)
            os.system("mv *.ASCATprofile.png " + s_opathascat)

        # sample processed
        print("0K\n")

    # set work directory back
    os.chdir(s_pwd)

    # output
    b_0k = True
    return("b_0K")


def ascatpcfed2igvseg(ldts_sample, s_pathmain, s_ipathlib, s_ipath="06ascatNN/", s_opath="txt_igvNN/", s_regex=""):
    """ manipulte ascat output """
    # run ascat
    # initialize output
    b_0K = False

    # handle input
    s_ipath = s_pathmain + s_ipath
    s_opath = s_pathmain + s_opath
    if not(os.path.isdir(s_opath)):
        os.mkdir(s_opath)

    # for each sample
    for dts_sample in ldts_sample:

        # regex processing filter
        o_regex = re.search(s_regex, dts_sample["mutant"][0])
        if (o_regex == None):
            print("\nSkipping ascatpcfed2igvseg {} because of regex or no ascat input ...".format(dts_sample))
        else:
            print("\nProcessing ascatpcfed2igvseg {} ...".format(dts_sample))

            # mutant
            try:
                ts_mutant = dts_sample["mutant"]
            except KeyError:
                sys.exit("Mutant sample missing, and only wid type sample can be missing.")

            # transfom pcf segemented output file into igv browser compatible seg file
            s_mutant = ts_mutant[0]

            # baf
            s_ifile = s_mutant + ".BAF.PCFed.txt"
            txtpcf2txtseg(s_ipath=s_ipath, s_ifile=s_ifile, s_ipathlib=s_ipathlib, s_isnpos="SNPpos.txt", s_opath=s_opath)

            # logr
            s_ifile = s_mutant + ".LogR.PCFed.txt"
            txtpcf2txtseg(s_ipath=s_ipath, s_ifile=s_ifile, s_ipathlib=s_ipathlib, s_isnpos="SNPpos.txt", s_opath=s_opath)

        # sample processed
        print("0K\n")

    # output
    b_0k = True
    return("b_0K")


def ascatmajorminor2igvseg(ldts_sample, s_pathmain, s_ipath="06ascatNN/",  s_opath="txt_igvNN/", s_regex=""):
    """
    input:
    output:
      - s_pathmain/txt_igvNN/*
    """
    # initialize output
    b_0k = False

    # handle input
    s_ipath = s_pathmain + s_ipath
    s_opath = s_pathmain + s_opath
    if not(os.path.isdir(s_opath)):
        os.mkdir(s_opath)

    # for each sample
    for dts_sample in ldts_sample:

        # regex processing filter
        o_regex = re.search(s_regex, dts_sample["mutant"][0])
        if (o_regex == None):
            print("\nSkipping ascatmajorminor2igvseg {} because of regex or no ascat input ...".format(dts_sample))
        else:
            print("\nProcessing ascatmajorminor2igvseg {} ...".format(dts_sample))

            # mutant
            try:
                ts_mutant = dts_sample["mutant"]
            except KeyError:
                sys.exit("Mutant sample missing, and only wid type sample can be missing.")

            # transfom pcf segemented output file into igv browser compatible seg file
            s_mutant = ts_mutant[0]

            # get filename
            s_ifile = ts_mutant[0] + ".segments.txt"
            s_ipathfile = s_ipath + s_ifile
            # call function
            ls_sample = txtmajorminor2txtseg(s_itxtmajorminor=s_ipathfile, s_opath=s_opath)

    # output
    b_0k = True
    return(b_0k)


def igvseg2segdiff(ldts_sample, s_pathmain, s_iopath="txt_igvNN/", s_regex=""):
    """
    input:
    output:
      - s_pathmain/txt_igvNN/*
    """
    # initialize output
    b_0k = False

    # handle input
    s_iopath = s_pathmain + s_iopath
    if not(os.path.isdir(s_iopath)):
        os.mkdir(s_iopath)

    # for each sample
    for dts_sample in ldts_sample:

        # regex processing filter
        o_regex = re.search(s_regex, dts_sample["mutant"][0])
        if (o_regex == None):
            print("\nSkipping igvseg2segdiff {} because of regex or no ascat input ...".format(dts_sample))
        else:
            print("\nProcessing igvseg2segdiff {} ...".format(dts_sample))

            # wildtype
            try:
                ts_wildtype = dts_sample["wildtype"]
                # mutant
                try:
                    ts_mutant = dts_sample["mutant"]

                    # get wt filename
                    s_mutant = ts_mutant[0]
                    s_itxtsegmt = s_iopath + s_mutant + ".LogR.PCFed.seg"
                    # get wt filename
                    s_wildtype = ts_wildtype[0]
                    s_itxtsegwt = s_iopath + s_wildtype + ".LogR.PCFed.seg"
                    # call function
                    ls_sample = txtsegdiff(s_itxtsegmt=s_itxtsegmt, s_itxtsegwt=s_itxtsegwt, s_opath=s_iopath)

                except KeyError:
                    sys.exit("Mutant sample missing, and only wid type sample can be missing.")

            except KeyError:
                print("\nSkipping igvseg2segdiff {} because no wildtype ...".format(dts_sample))

    # output
    b_0k = True
    return(b_0k)

def ascatmajorminor2fasta(ldts_sample, s_pathmain, s_ipath="06ascatNN/", s_opathigv="txt_igvNN/", s_opathmedicc="mediccNN_input/", s_otxtprefix="analysis", s_regex=""):
    """
    input:
    output:
      - s_pathmain/mediccNN_input/*
    """
    # initialize output
    b_0k = False

    # handle input
    s_ipath = s_pathmain + s_ipath
    s_opathigv = s_pathmain + s_opathigv
    if not(os.path.isdir(s_opathigv)):
        os.mkdir(s_opathigv)
    s_opathmedicc = s_pathmain + s_opathmedicc
    if not(os.path.isdir(s_opathmedicc)):
        os.mkdir(s_opathmedicc)

    # for each sample
    ls_ifile = []
    for dts_sample in ldts_sample:

        # regex processing filter
        o_regex = re.search(s_regex, dts_sample["mutant"][0])
        if (o_regex == None):
            print("\nSkipping ascatmajorminor2fasta {} because of regex or no ascat input ...".format(dts_sample))
        else:
            print("\nProcessing ascatmajorminor2fasta {} ...".format(dts_sample))

            # mutant
            try:
                ts_mutant = dts_sample["mutant"]
            except KeyError:
                sys.exit("Mutant sample missing, and only wid type sample can be missing.")

            # transfom pcf segemented output file into igv browser compatible seg file
            s_mutant = ts_mutant[0]

            # get filename
            s_ifile = ts_mutant[0] + ".segments.txt"
            s_ipathfile = s_ipath + s_ifile
            ls_ifile.append(s_ipathfile)
    # call function
    ls_ofile = txtmajorminor2txtfasta(ls_itxtmajorminor=ls_ifile,  s_opathigv=s_opathigv, s_opathmedicc=s_opathmedicc, s_otxtprefix=s_otxtprefix)
    print("FASTA", ls_ofile)

    # output
    b_0k = True
    return(b_0k)
