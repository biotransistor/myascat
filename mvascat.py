# bue 20160703: hack for folder structure
import os

# set work directory
s_opath = "./"

# set  output subpaths
s_opathraw = s_opath + "raw/"
s_opathpcf = s_opath + "pcf_segmentation/"
s_opathploidyaberration = s_opath + "ascat_ploidyaberration/"
s_opathminormajor = s_opath + "ascat_minormajor/"
s_opathascat =  s_opath + "ascat_profile/"

print("move...")

# move raw plots
if not(os.path.isdir(s_opathraw)):
    os.mkdir(s_opathraw)
os.system("mv *.tumour.png " + s_opathraw)
os.system("mv *.germline.png " + s_opathraw)
os.system("mv tumorSep*.png " + s_opathraw)

# move segementation plots
if not(os.path.isdir(s_opathpcf)):
    os.mkdir(s_opathpcf)
os.system("mv *.ASPCF.png " + s_opathpcf)

# move ascat plots ascat_ploidyaberration
if not(os.path.isdir(s_opathploidyaberration)):
    os.mkdir(s_opathploidyaberration)
os.system("mv *.aberrationreliability.png " + s_opathploidyaberration)
os.system("mv *.sunrise.png " + s_opathploidyaberration)

# move ascat plot ascat_minormajor
if not(os.path.isdir(s_opathminormajor)):
    os.mkdir(s_opathminormajor)
os.system("mv *.rawprofile.png " + s_opathminormajor)

# move ascat plot ascat_profile
if not(os.path.isdir(s_opathascat)):
    os.mkdir(s_opathascat)
os.system("mv *.ASCATprofile.png " + s_opathascat)

print("0k\n")
