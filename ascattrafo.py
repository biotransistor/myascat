#import pandas as pd
import csv
import os
import sys
# scipy libraries
import pandas as pd


#####################
# internal methodes #
#####################
# bue 2016-06-21: evt import retype
# bue 2016-06-21: evt sort key fuction chromosome needed

# bue unittest 0k
def txtseg2d2t(s_itxtseg):
    """
    descrription:
      read igv seg file into dictoionary of dictionary object

    input:
      - s_itxtseg file

    output:
      - d2t_seg dictionary of dictionary
    """
    d2t_seg = {}
    s_thetrack = None
    # read file
    with open(s_itxtseg, newline='') as f_in:
        b_header0k = False
        reader = csv.reader(f_in, delimiter="\t")
        for ls_row in reader:
            if not (b_header0k):
                ls_header = ls_row
                b_header0k = True
            else:
                s_track = ls_row[0]
                if (s_thetrack == None):
                    s_thetrack = s_track
                s_chromosome = ls_row[1]
                i_begin = int(ls_row[2])
                i_end = int(ls_row[3])
                r_value = float(ls_row[-1])
                if (len(ls_row) > 5):
                    s_label = ls_row[4]
                    if (s_label == 'None'):
                        s_label = None
                else:
                    s_label = None
                # write dict
                if (s_track != s_thetrack):
                    sys.exit("Error: file {} contains entry form more then one track, namely {} and {}".format(s_itxtseg, s_thetrack, s_track))
                t_entry = (i_end, r_value, s_label, s_thetrack)
                try:
                    dt_seg = d2t_seg[s_chromosome]
                    d2t_seg[s_chromosome].update({i_begin:t_entry})
                except KeyError:
                    d2t_seg.update({s_chromosome:{i_begin:t_entry}})
    # output
    return(d2t_seg)


# bue unittest 0k
def d2t2txtseg(d2t_seg, s_opath):
    """
    descrription:
      read igv seg file into dictoionary of dictionary object

    input:
      - d2t_seg dictionary of dictionary
      - s_opath: outout path where seg file gets written to.

    output:
      - s_itxtseg file
    """
    s_ofile = None
    # get file name
    ls_chromosome = list(d2t_seg.keys())
    ls_chromosome.sort()
    s_chromosome = ls_chromosome[0]
    li_begin = list(d2t_seg[s_chromosome].keys())
    i_begin = li_begin[0]
    s_track = d2t_seg[s_chromosome][i_begin][3]
    s_ofile = s_opath + s_track + ".seg"
    # write dict to file
    with open(s_ofile, 'w', newline='') as f_out:
        writer = csv.writer(f_out, delimiter="\t")
        writer.writerow(("track","chromosome","begin","end","label","value"))
        for s_chromosome in ls_chromosome:
            li_begin = list(d2t_seg[s_chromosome])
            li_begin.sort()
            for i_begin in li_begin:
                t_entry = d2t_seg[s_chromosome][i_begin]
                i_end = t_entry[0]
                r_value = t_entry[1]
                s_label = t_entry[2]
                if (s_label == None):
                    s_label = "None"
                s_track = t_entry[3]
                # write line
                writer.writerow((s_track, s_chromosome,i_begin,i_end, s_label, r_value))
    # output
    return(s_ofile)


# bue unittest 0k
def trackintersection(s_thechromosome, i_thebegin, i_theend, d2t_track, s_track=None):
    """
    *internal_internal*
    s_thechromosome, i_thebegin, i_theend, intersect
    d2t_track {s_chromosome : i_begin : (i_end, amplitude, label, track)}}}

    output:
        d2t_newtrack
    """
    d2t_newtrack = {}

    # get new beginnings
    li_begin = list(d2t_track[s_thechromosome].keys())
    li_begin.sort()
    for i_begin in li_begin:
        b_populate = False
        t_entry = d2t_track[s_thechromosome][i_begin]
        i_end = t_entry[0]
        r_newamplitude = t_entry[1]
        s_newlabel = t_entry[2]
        if (s_track == None):
            s_newtrack = t_entry[3]
        else:
            s_newtrack = s_track

        if (i_begin <= i_thebegin) and  (i_end <= i_theend) and (i_begin <= i_theend) and (i_end >= i_thebegin):
            #print("A begin befor master and end  befor master")
            i_newbegin = i_thebegin
            i_newend = i_end
            # set output flag
            b_populate = True

        elif (i_begin <= i_thebegin) and  (i_end >= i_theend) and (i_begin <= i_theend) and (i_end >= i_thebegin):
            #print("B begin befor master and end after master")
            t_entry = d2t_track[s_thechromosome][i_begin]
            i_newbegin = i_thebegin
            i_newend = i_theend
            # set output flag
            b_populate = True

        elif (i_begin >= i_thebegin) and (i_end <= i_theend) and (i_begin <= i_theend) and (i_end >= i_thebegin):
            #print("C begin after master and end before master")
            t_entry = d2t_track[s_thechromosome][i_begin]
            i_newbegin = i_begin
            i_newend = i_end
            # set output flag
            b_populate = True

        elif (i_begin >= i_thebegin) and  (i_end >= i_theend) and (i_begin <= i_theend) and (i_end >= i_thebegin):
            #print("D begin after master and end after master")
            t_entry = d2t_track[s_thechromosome][i_begin]
            i_newbegin = i_begin
            i_newend = i_theend
            # set output flag
            b_populate = True

        elif (i_end <= i_thebegin):
            #print("NOP begin after master end")
            pass
        elif (i_begin >= i_theend):
            #print("NOP end befor master begin")
            pass
        else:
          sys.exit("Error: what the heck! end befor begin? begin after end?")

        # populate output
        if(b_populate):
            t_newentry = (i_newend, r_newamplitude, s_newlabel, s_newtrack)
            try:
                d2t_newtrack[s_thechromosome].update({i_newbegin : t_newentry})
            except KeyError:
                d2t_newtrack.update({s_thechromosome : { i_newbegin : t_newentry}})
    # output
    return(d2t_newtrack)


# bue unittest 0k
def d2ttrackdiff(d2t_mt, d2t_wt, s_track=None):
    """
    get intersecting difference
    """
    # set output dict
    d2t_diff = {}
    # get track name
    if (s_track == None):
        # mt
        s_mtchromosome = list(d2t_mt.keys())[0]
        s_mtbegin = list(d2t_mt[s_mtchromosome].keys())[0]
        s_mttrack = d2t_mt[s_mtchromosome][s_mtbegin][3]
        # wt
        s_wtchromosome = list(d2t_wt.keys())[0]
        s_wtbegin = list(d2t_wt[s_wtchromosome].keys())[0]
        s_wttrack = d2t_wt[s_wtchromosome][s_wtbegin][3]
        # fusion
        s_track = "difftrack_"+s_mttrack+"-"+s_wttrack
    # loop
    ls_chromosome = list(d2t_wt.keys())
    for s_chromosome in ls_chromosome:
        li_begin = list(d2t_wt[s_chromosome].keys())
        for i_begin in li_begin:
            # get wt entry
            t_wtentry = d2t_wt[s_chromosome][i_begin]
            i_end = t_wtentry[0]
            r_wtamp = t_wtentry[1]
            s_wtlabel = t_wtentry[2]
            s_wttrack = t_wtentry[3]
            # intersection
            d2t_intersection = trackintersection(s_thechromosome=s_chromosome, i_thebegin=i_begin, i_theend=i_end, d2t_track=d2t_mt, s_track=s_track)
            li_newbegin = list(d2t_intersection[s_chromosome].keys())
            for i_newbegin in li_newbegin:
                l_newentry = list(d2t_intersection[s_chromosome][i_newbegin])
                r_mtamp = l_newentry[1]
                r_newamp = r_mtamp - r_wtamp
                l_newentry[1] = r_newamp
                try:
                    d2t_diff[s_chromosome].update({i_newbegin : tuple(l_newentry)})
                except KeyError:
                    d2t_diff.update({s_chromosome:{i_newbegin : tuple(l_newentry)}})
    # output
    return(d2t_diff)


# bue unittest 0k
def d3ttrack2intersection(d3t_track, d2t_filter=None):
    """
    *internal*
    """
    # get samples
    d3t_out = {}
    ls_sample = list(d3t_track.keys())
    s_master = ls_sample.pop()

    # apply filter on newmaster
    if (d2t_filter == None):
        #d2t_master = {}
        d2t_newmaster = d3t_track[s_master]
        #ls_filterchromosome = list(d2t_master.keys())
    else:
        d2t_master = d3t_track[s_master]
        d2t_newmaster = {}
        ls_chromosome = list(d2t_master.keys())
        ls_filterchromosome = list(d2t_filter.keys())
        for s_chromosome in ls_filterchromosome:
            if (s_chromosome in ls_filterchromosome):
                li_begin = d2t_master[s_chromosome]
                for i_begin in li_begin:
                    # is begin less then filter end
                    i_filterend = d2t_filter[s_chromosome][1]
                    if (i_begin <= i_filterend):
                        # end greater then filter begin
                        i_filterbergin =  d2t_filter[s_chromosome][0]
                        t_theentry = d2t_master[s_chromosome][i_begin]
                        i_end = t_theentry[0]
                        if (i_end >= i_filterbergin):
                            # the begin
                            i_thebegin = i_begin
                            if (i_begin < i_filterbergin):
                                i_thebegin = i_filterbergin
                            # the end
                            if (i_end > i_filterend):
                                l_theentry = list(t_theentry)
                                l_theentry[0] = i_filterend
                                t_theentry = tuple(l_theentry)
                            # popolate newmaster
                            try:
                                d2t_newmaster[s_chromosome].update({i_thebegin : t_theentry})
                            except KeyError:
                                d2t_newmaster.update({s_chromosome:{i_thebegin : t_theentry}})

    # intersect master
    for s_sample in ls_sample:
        d2t_master = d2t_newmaster.copy()
        ls_chromosome = list(d2t_master.keys())
        d2t_newmaster = {}
        for s_chromosome in ls_chromosome:
            li_begin = list(d3t_track[s_sample][s_chromosome].keys())
            for i_thebegin in li_begin:
                t_entry = d3t_track[s_sample][s_chromosome][i_thebegin]
                i_theend = t_entry[0]
                d2t_intersection = trackintersection(s_thechromosome=s_chromosome, i_thebegin=i_thebegin, i_theend=i_theend, d2t_track=d2t_master)
                if (d2t_intersection != {}):
                    li_newbegin = list(d2t_intersection[s_chromosome].keys())
                    for i_newbegin in li_newbegin:
                        t_newentry = d2t_intersection[s_chromosome][i_newbegin]
                        # populate new master
                        try:
                            d2t_newmaster[s_chromosome].update({i_newbegin : t_newentry})
                        except KeyError:
                            d2t_newmaster.update({s_chromosome:{i_newbegin : t_newentry}})

    # populate output
    d2t_master = d2t_newmaster
    d3t_out.update({s_master : d2t_master})

    # intersect slaves
    ls_chromosome = list(d2t_master.keys())
    for s_sample in ls_sample:
        d2t_out = {}
        d2t_slave = d3t_track[s_sample]
        for s_chromosome in ls_chromosome:
            li_begin = list(d2t_master[s_chromosome].keys())
            for i_thebegin in li_begin:
                t_entry = d2t_master[s_chromosome][i_thebegin]
                i_theend = t_entry[0]
                d2t_intersection = trackintersection(s_thechromosome=s_chromosome, i_thebegin=i_thebegin, i_theend=i_theend, d2t_track=d2t_slave)
                li_newbegin = list(d2t_intersection[s_chromosome].keys())
                for i_newbegin in li_newbegin:
                    t_newentry = d2t_intersection[s_chromosome][i_newbegin]
                    try:
                        d2t_out[s_chromosome].update({i_newbegin : t_newentry})
                    except KeyError:
                        d2t_out.update({s_chromosome:{i_newbegin : t_newentry}})
        # populate output
        d3t_out.update({s_sample : d2t_out})

    # output
    return(d3t_out)


# bue unittest 0kd3intersected2txt
def d3intersected2txtfasta(d3t_intersected, s_maijnor, s_otxtprefix, s_opath):
    """
    *internal*
    """
    ls_descr = None
    # populate output dictionary
    d_out={}
    ls_sample = list(d3t_intersected.keys())
    ls_sample.sort()
    for s_sample in ls_sample:
        s_seqenceid = s_sample.replace("_"+s_maijnor,"")
        ls_chromosom = list(d3t_intersected[s_sample].keys())
        ls_chromosom.sort()
        for s_chromosome in ls_chromosom:
            s_ofile = s_otxtprefix+"_"+s_chromosome+"_"+s_maijnor+".fasta"
            li_begin = list(d3t_intersected[s_sample][s_chromosome].keys())
            li_begin.sort(key=int)
            try:
                s_output = d_out[s_ofile]
            except KeyError:
                # for first diploid sample
                s_output = ">diploid\n" + "1"*len(li_begin) + "\n"
            # any sample
            s_out = ">"+s_seqenceid+"\n"
            for i_begin in li_begin:
                # real sample
                t_entry = d3t_intersected[s_sample][s_chromosome][i_begin]
                r_value = t_entry[1]
                if (r_value < -4):
                    r_value = -4
                elif (r_value > 4):
                    r_value = 4
                s_out = s_out + str(int(r_value))
            # populate output dictionary
            s_output = s_output + s_out + "\n"
            d_out.update({s_ofile:s_output})

    # write output dictionary into txt fasta files
    ls_ofile = list(d_out.keys())
    ls_ofile.sort()
    for s_ofile in ls_ofile:
        s_opathfile = s_opath + s_ofile
        s_output = d_out[s_ofile]
        with open(s_opathfile, 'w') as f_out:
            f_out.write(s_output)

    # output
    ls_descr = ls_ofile
    return(ls_descr)


#################
# main methodes #
#################
# bue unittest 0k
def txtsegdiff(s_itxtsegmt, s_itxtsegwt, s_opath):
    """
    input: igv
    output: igv
    """
    b_0k = False
    d2t_mt = txtseg2d2t(s_itxtseg=s_itxtsegmt)
    d2t_wt = txtseg2d2t(s_itxtseg=s_itxtsegwt)
    d2t_diff = d2ttrackdiff(d2t_mt=d2t_mt, d2t_wt=d2t_wt)
    s_ofilen = d2t2txtseg(d2t_seg=d2t_diff, s_opath=s_opath)
    return(s_ofilen)


# bue unittest 0k
def txtmajorminor2txtseg(s_itxtmajorminor, s_opath):
    """
    input: ascat segements
    output: igv
    """
    ls_ofilen = None
    s_thelabel = None
    d2t_major = {}
    d2t_minor = {}

    # get ascat segmentation file
    d2t_majorminor = txtseg2d2t(s_itxtseg=s_itxtmajorminor)
    ls_chromosome = list(d2t_majorminor.keys())
    if (len(ls_chromosome) > 0):
        for s_chromosome in ls_chromosome:
            li_begin = list(d2t_majorminor[s_chromosome].keys())
            for i_begin in li_begin:
                # get input
                t_entry = d2t_majorminor[s_chromosome][i_begin]
                i_end = t_entry[0]
                r_minor = float(t_entry[1])
                r_major = float(t_entry[2])
                # populate major output
                s_track = t_entry[3] + ".major"
                try:
                    d2t_major[s_chromosome].update({i_begin:(i_end, r_major, s_thelabel, s_track)})
                except KeyError:
                    d2t_major.update({s_chromosome:{i_begin:(i_end, r_major, s_thelabel, s_track)}})
                # populate minor output
                s_track = t_entry[3] + ".minor"
                try:
                    d2t_minor[s_chromosome].update({i_begin:(i_end, r_minor, s_thelabel, s_track)})
                except KeyError:
                    d2t_minor.update({s_chromosome:{i_begin:(i_end, r_minor, s_thelabel, s_track)}})

        # put igv seg fiels for major and minor alle
        ls_ofilen = []
        s_ofilen = d2t2txtseg(d2t_seg=d2t_major, s_opath=s_opath)
        ls_ofilen.append(s_ofilen)
        s_ofilen = d2t2txtseg(d2t_seg=d2t_minor, s_opath=s_opath)
        ls_ofilen.append(s_ofilen)
    # output
    return(ls_ofilen)

# bue unittest 0k
def txtmajorminor2txtfasta(ls_itxtmajorminor, s_otxtprefix, s_opathigv="igvNN/", s_opathmedicc="mediccNN_input/", d2t_filter=None):
    """
    input: ascat segemnts
    output: medicc
    """
    b_0k = False
    d3t_major = {}
    d3t_minor = {}
    # get ascat segmentation file
    for s_itxtmajorminor in ls_itxtmajorminor:
        # transform ascat majorminor file into seg files
        if not (os.path.isdir(s_opathigv)):
            os.mkdir(s_opathigv)
        ls_filemajorminor=txtmajorminor2txtseg(s_itxtmajorminor, s_opath=s_opathigv)
        if (ls_filemajorminor != None):
            # get sample name
            s_sample = s_itxtmajorminor.split('/')[-1].split('.')[0]
            # read seg files into major and minor dictionary of dictionary
            # major
            d2t_major=txtseg2d2t(ls_filemajorminor[0])
            d3t_major.update({s_sample+"_major" : d2t_major})
            # minor
            d2t_minor=txtseg2d2t(ls_filemajorminor[1])
            d3t_minor.update({s_sample+"_minor" : d2t_minor})
    # get intersecting segements
    if (d3t_major != {}) and (d3t_minor != {}):
        d3t_majorintersected = d3ttrack2intersection(d3t_track=d3t_major, d2t_filter=d2t_filter)
        d3t_minorintersected = d3ttrack2intersection(d3t_track=d3t_minor, d2t_filter=d2t_filter)
        # put medicc txt fasta files
        if not (os.path.isdir(s_opathmedicc)):
            os.mkdir(s_opathmedicc)
        ls_major = d3intersected2txtfasta(d3t_intersected=d3t_majorintersected, s_maijnor="major", s_otxtprefix=s_otxtprefix, s_opath=s_opathmedicc)
        ls_minor = d3intersected2txtfasta(d3t_intersected=d3t_minorintersected, s_maijnor="minor", s_otxtprefix=s_otxtprefix, s_opath=s_opathmedicc)
        # put medicc txt description file
        s_ofiles = s_opathmedicc+s_otxtprefix+"_descr.txt"
        with open(s_ofiles, 'w') as f_out:
            for i_index in range(len(ls_major)):
                s_majorfile = ls_major[i_index]
                s_minorfile = ls_minor[i_index]
                s_chromosome = s_majorfile.split("_")[-2]
                s_output = s_chromosome+" "+ls_major[i_index]+" "+ls_minor[i_index]+"\n"
                f_out.write(s_output)
        # output
        b_0k = True
    # return
    return(b_0k)


# bue unittest NOT 0k
'''
def txtfasta2txtseg(s_itxtprefix=None, s_ipath""):
    """
    input: medicc
    output: igv
    """
    b_0k=False
    if (s_itxtprefix == None):
        s_itxtprefix = "medicc"
    # get all B allele and B allele fasta file for each chromosome
    # put all A allele and B allele seq file for each sample
        s_oafile = s_itxtprefix + ".aallele.seq"
        s_obfile = s_itxtprefix + ".ballele.seq"
    return(b_0k)

example1.out/chrom1/allelesA.fasta
example1.out/chrom1/allelesB.fasta
example1.out/chrom2/allelesA.fasta
example1.out/chrom2/allelesB.fasta
'''
