import unittest
import shutil
from myascat.ascat import cel2logrbaf, logrbaf2ascat, ascatpcfed2igvseg, logrbaf2plotallele, logrbaf2plotgamma, ascatmajorminor2igvseg, igvseg2segdiff, ascatmajorminor2fasta


class TestAscat(unittest.TestCase):
    def setUp(self):
        """
        set input variables
        """
        # paths
        self.s_ifileprj = "testifileprj0k.txt"
        self.s_ipathprj = "/Users/buchere/data_work/20090402hapmap3_snp/cel/"
        self.s_ifilehapmap = "testifilehapmap0k.txt"
        self.s_ipathhapmap = "/Users/buchere/data_work/20090402hapmap3_snp/cel/"
        self.s_ipathlib = "/Users/buchere/src_gitlab/myascat/srcsnp6/lib/"
        self.i_pcfgamma = 40  # default 40
        self.b_jump_cel2logrbaf = False
        self.b_jump_lrrbaf2logrbaf = False
        self.b_jump_plotallele = False
        self.b_jump_plotgamma = False
        self.b_jump_ascat = False
        self.b_jump_ascatpcfed2igvseg = False
        self.b_jump_ascatmajorminor2igvseg = False
        self.b_jump_igvseg2segdiff = False
        self.b_jump_ascatmajorminor2fasta = False

        # sample
        self.ldts_sample = []
        # sample mutant only
        dts_entry = {}
        dts_entry.update({"mutant":("mutant_SHELF_H08_SNP6","SHELF_g_GAINmixHapMapAffy3_GenomeWideEx_6_H08_31536.CEL","XX")})
        self.ldts_sample.append(dts_entry)
        # sample mutant only
        dts_entry = {}
        dts_entry.update({"mutant":("mutant_SHELF_H09_SNP6","SHELF_g_GAINmixHapMapAffy3_GenomeWideEx_6_H09_31552.CEL","XX")})
        self.ldts_sample.append(dts_entry)
        # sample with wildtype as mutant
        dts_entry = {}
        dts_entry.update({"mutant":("wildtype_SHELF_H10_SNP6","SHELF_g_GAINmixHapMapAffy3_GenomeWideEx_6_H10_31568.CEL", "XX")})
        self.ldts_sample.append(dts_entry)
        # sample wildtype mutant
        dts_entry = {}
        dts_entry.update({"wildtype":("wildtype_SHELF_H10_SNP6","SHELF_g_GAINmixHapMapAffy3_GenomeWideEx_6_H10_31568.CEL", "XX")})
        dts_entry.update({"mutant":("mutant_SHELF_H11_SNP6","SHELF_g_GAINmixHapMapAffy3_GenomeWideEx_6_H11_31584.CEL", "XX")})
        self.ldts_sample.append(dts_entry)
        # sample mutant only
        dts_entry = {}
        dts_entry.update({"mutant":("mutant_SHELF_H12_SNP6","SHELF_g_GAINmixHapMapAffy3_GenomeWideEx_6_H12_31600.CEL", None, 0.5, 2, 33)})
        self.ldts_sample.append(dts_entry)
        # sample mutant only
        dts_entry = {}
        dts_entry.update({"mutant":("pcftrack","NOP","XX")})
        self.ldts_sample.append(dts_entry)



    def test_pipeline(self):
        #######################
        ### cel to logrbaf ####
        #######################
        """"
        run ascat pipeline form cell files to logR BAF files

        input:
          - mutant or mutant and wildtype CELL files

        output:
          - 01getcel: listing of gathered prj and hapmat cellfiles
          - 02aptprobsetgenotype: apt-probeset-genotype command output
          - 03aptprobesetsummarizes: apt-probeset-summarize command output
          - 04penncnvnormalizeaffygenocluster: normalize_affy_geno_cluster.pl command output
          - 05lrrbaf2logrbaf: LogR and BAF files

        """
        b_0k = False
        ### set samples by regex ###
        self.s_regex = "_SHELF_"
        ### set test output folder ###
        self.s_pathmain = "/Users/buchere/src_gitlab/myascat/test_inout/"
        if not ((self.b_jump_lrrbaf2logrbaf) and (self.b_jump_cel2logrbaf)):
            ### clear output ###
            shutil.rmtree(self.s_pathmain+"01getcel", ignore_errors=True)
            if not (self.b_jump_cel2logrbaf):
                shutil.rmtree(self.s_pathmain+"02aptprobsetgenotype", ignore_errors=True)
                shutil.rmtree(self.s_pathmain+"03aptprobesetsummarizes", ignore_errors=True)
                shutil.rmtree(self.s_pathmain+"04penncnvnormalizeaffygenocluster", ignore_errors=True)
            shutil.rmtree(self.s_pathmain+"05lrrbaf2logrbaf", ignore_errors=True)
            ### run code ###
            b_0k = cel2logrbaf(ldts_sample=self.ldts_sample, s_pathmain=self.s_pathmain, s_ifileprj=self.s_ifileprj, s_ipathprj=self.s_ipathprj, s_ifilehapmap=self.s_ifilehapmap, s_ipathhapmap=self.s_ipathhapmap, s_ipathlib=self.s_ipathlib, s_regex=self.s_regex, b_jump=self.b_jump_cel2logrbaf)
            self.assertTrue(b_0k)


        #########################
        # logrbaf to plotallele #
        #########################
        """
        run ascat of ascat pipeline

        input:
          - 05lrrbaf2logrbaf: LogR and BAF files

        output:
          - 06pcf40: all standar ascat output
        """
        b_plotallele = False
        if not (self.b_jump_plotallele):
            ls_result = ['pcftrack_LogR']
            # set samples by regex ###
            self.s_regex = "pcftrack"
            ### set test input output folder ###
            self.s_pathmain = "/Users/buchere/src_gitlab/myascat/test_inout/"
            ### run code ###
            ls_samples = logrbaf2plotallele(ldts_sample=self.ldts_sample, s_pathmain=self.s_pathmain, s_ipath="05lrrbaf2logrbafIn/", s_regex=self.s_regex)
            print(ls_samples)
            self.assertEqual(ls_samples, ls_result)


        ########################
        # logrbaf to plotgamma #
        ########################
        """
        run ascat of ascat pipeline

        input:
          - 05lrrbaf2logrbaf: LogR and BAF files

        output:
          - 06pcf: all standar ascat output
        """
        b_plotallele = False
        if not (self.b_jump_plotgamma):
            ls_result = ['pcftrack_LogR.chromosome1', 'pcftrack_LogR.chromosome2', 'pcftrack_LogR.chromosome3', 'pcftrack_LogR.chromosome4', 'pcftrack_LogR.chromosome5', 'pcftrack_LogR.chromosome6', 'pcftrack_LogR.chromosome7', 'pcftrack_LogR.chromosome8', 'pcftrack_LogR.chromosome9', 'pcftrack_LogR.chromosome10', 'pcftrack_LogR.chromosome11', 'pcftrack_LogR.chromosome12', 'pcftrack_LogR.chromosome13', 'pcftrack_LogR.chromosome14', 'pcftrack_LogR.chromosome15', 'pcftrack_LogR.chromosome16', 'pcftrack_LogR.chromosome17', 'pcftrack_LogR.chromosome18', 'pcftrack_LogR.chromosome19', 'pcftrack_LogR.chromosome20', 'pcftrack_LogR.chromosome21', 'pcftrack_LogR.chromosome22', 'pcftrack_LogR.chromosome23']
            ### set samples by regex ###
            self.s_regex = "pcftrack"
            ### set test input output folder ###
            self.s_pathmain = "/Users/buchere/src_gitlab/myascat/test_inout/"
            ### run code ###
            ls_samples = logrbaf2plotgamma(ldts_sample=self.ldts_sample, s_pathmain=self.s_pathmain, s_ipath="05lrrbaf2logrbafIn/", s_regex=self.s_regex)
            print(ls_samples)


        ####################
        # logrbaf to ascat #
        ####################
        """
        run ascat of ascat pipeline

        input:
          - 05lrrbaf2logrbaf: LogR and BAF files

        output:
          - 06ascat40: all standar ascat output
        """
        # bue 201603: as ascat example input files have
        # non affymetrix snp6 chip compatible snp names,
        # ascat gc correct code will brake, if b_gccorrect = True
        b_ascat = False
        if not (self.b_jump_ascat):
            ### set samples by regex ###
            self.s_regex = "_SHELF_"
            ### set test input output folder ###
            self.s_pathmain = "/Users/buchere/src_gitlab/myascat/test_inout/"
            ### clear output ###
            shutil.rmtree(self.s_pathmain+"06ascat"+str(self.i_pcfgamma), ignore_errors=True)
            ### run code ###
            b_ascat = logrbaf2ascat(ldts_sample=self.ldts_sample, s_pathmain=self.s_pathmain, s_ipathlib=self.s_ipathlib, i_pcfgamma=self.i_pcfgamma, b_gccorrect=False, s_regex=self.s_regex)
            self.assertTrue(b_ascat)


        ########################
        # ascatpcfed to igvseg #
        ########################
        """
        run ascat pipeline from stanadrd ascat output to igv genome browser compatibe input

        input:
          - 06ascat40: ascat standard output

        output:
          - txt_igv40: igv compativle seg files
        """
        b_0k = False
        if not (self.b_jump_ascatpcfed2igvseg):
            ### set samples by regex ###
            self.s_regex = "_SHELF_"
            # set test input output folder
            self.s_pathmain = "/Users/buchere/src_gitlab/myascat/test_inout/"
            # clear output
            shutil.rmtree(self.s_pathmain+"txt_igv40/", ignore_errors=True)
            # run code
            s_out = "txt_igv" + str(self.i_pcfgamma) + "/"
            b_0k = ascatpcfed2igvseg(ldts_sample=self.ldts_sample, s_pathmain=self.s_pathmain, s_ipathlib=self.s_ipathlib, s_ipath="06ascatIn/", s_opath=s_out, s_regex=self.s_regex)
            self.assertTrue(b_0k)


        #############################
        # ascatmajorminor to igvseg #
        #############################
        """
        run ascat pipeline from stanadrd ascat output to igv genome browser compatibe input

        input:
          - 06ascat40: ascat standard output

        output:
          - txt_igv40: igv compativle seg files
        """
        b_0k = False
        if not (self.b_jump_ascatmajorminor2igvseg):
            ### set samples by regex ###
            self.s_regex = "_SHELF_"
            # set test input output folder
            self.s_pathmain = "/Users/buchere/src_gitlab/myascat/test_inout/"
            # run code
            s_out = "txt_igv" + str(self.i_pcfgamma) + "/"
            b_0k = ascatmajorminor2igvseg(ldts_sample=self.ldts_sample, s_pathmain=self.s_pathmain, s_ipath="06ascatIn/",  s_opath=s_out, s_regex=self.s_regex)
            self.assertTrue(b_0k)


        #####################
        # igvseg to segdiff #
        #####################
        """
        run ascat pipeline from stanadrd ascat output to igv genome browser compatibe input

        input:
          - 06ascat40: ascat standard output

        output:
          - txt_igv40: igv compativle seg files
        """
        b_0k = False
        if not (self.b_jump_igvseg2segdiff):
            ### set samples by regex ###
            self.s_regex = "_SHELF_"
            # set test input output folder
            self.s_pathmain = "/Users/buchere/src_gitlab/myascat/test_inout/"
            # run code
            b_0k = igvseg2segdiff(ldts_sample=self.ldts_sample, s_pathmain=self.s_pathmain, s_iopath="txt_igvIn/", s_regex=self.s_regex)
            self.assertTrue(b_0k)


        #############################
        # ascatmajorminor to fasta #
        #############################
        """
        run ascat pipeline from stanadrd ascat output to igv genome browser compatibe input

        input:
          - 06ascat40: ascat standard output

        output:
          - medicc40_input: meddic compatible fasta files
        """
        b_0k = False
        if not (self.b_jump_ascatmajorminor2fasta):
            ### set samples by regex ###
            self.s_regex = "_SHELF_"
            # set test input output folder
            self.s_pathmain = "/Users/buchere/src_gitlab/myascat/test_inout/"
            # run code
            s_outigv = "txt_igv" + str(self.i_pcfgamma) + "/"
            s_outmedicc = "medicc" + str(self.i_pcfgamma) + "_input/"
            b_0k = ascatmajorminor2fasta(ldts_sample=self.ldts_sample, s_pathmain=self.s_pathmain, s_ipath="06ascatIn/", s_opathigv=s_outigv, s_opathmedicc=s_outmedicc, s_otxtprefix="unittestascat", s_regex=self.s_regex)
            self.assertTrue(b_0k)

if __name__ == '__main__':
    unittest.main(warnings='ignore')
