import os

# R over rpy2 libararies
import rpy2.robjects as robjects
robjects.r("library(copynumber)")  # pcf


def plotallele(s_logr, s_baf, s_pathmain, s_ipath="05lrrbaf2logrbaf/", i_gamma=40, s_assembly="hg19", b_verbose=True):
    """
    gamma: penalty for each discontinuity in the curve
    ascat defaut gamma value: 25
    aspcf defaut gamma value: 40
    """
    ls_sample = []
    s_opath = s_pathmain + "06pcf" + str(i_gamma) + "/"

    # load BAF file
    s_command = "df_baf <- read.table('" + s_ipath + s_baf + "', sep='\t', header=TRUE, row.names=1)"
    robjects.r(s_command)

    # load logR file
    s_command = "df_logr <- read.table('" + s_ipath + s_logr + "', sep='\t', header=TRUE, row.names=1)"
    robjects.r(s_command)

    # handel outlier
    if not (b_verbose):
        s_command = "ldf_logrwinsor <- winsorize(df_logr, assembly='" + s_assembly + "', return.outliers=TRUE, verbose=FALSE)"
    else:
        s_command = "ldf_logrwinsor <- winsorize(df_logr, assembly='" + s_assembly + "', return.outliers=TRUE, verbose=TRUE)"
    robjects.r(s_command)

    # pcf segementation
    if not (b_verbose):
        s_command = "df_aspcf <- aspcf(ldf_logrwinsor[[1]], df_baf, gamma=" + str(i_gamma) + ", assembly='" + s_assembly + "', verbose=FALSE)"
    else:
        s_command = "df_aspcf <- aspcf(ldf_logrwinsor[[1]], df_baf, gamma=" + str(i_gamma) + ", assembly='" + s_assembly + "', verbose=TRUE)"
    robjects.r(s_command)

    # plotting
    if not (os.path.isdir(s_opath)):
        os.mkdir(s_opath)
    s_sample = s_logr.split("/")[-1].split(".")[0]
    ls_sample.append(s_sample)
    s_opathfile = s_opath + s_sample + ".pcf.plotallele.pdf"
    s_command = "plotAllele(logR=ldf_logrwinsor[[1]], BAF=df_baf, segments=df_aspcf, sample=1, chrom=c(1:23), assembly='" + s_assembly + "', winsoutliers=ldf_logrwinsor[[2]], layout=c(6,4))"
    robjects.r("pdf('" + s_opathfile + "')")
    robjects.r(s_command)
    robjects.r("dev.off()")
    # out
    return(ls_sample)


def plotgamma(s_trackfile, s_pathmain, s_ipath="05lrrbaf2logrbaf/", li_gammarange=[10,100], s_assembly="hg19"):
    """
    s_trackfile: logr or baf tack file
    """
    ls_sample = []
    s_opath = s_pathmain + "06pcf/"
    s_sample = s_trackfile.split("/")[-1].split(".")[0]
    if not (os.path.isdir(s_opath)):
        os.mkdir(s_opath)

    # load logR file
    s_command = "df_track <- read.table('"+ s_ipath + s_trackfile + "', sep='\t', header=TRUE, row.names=1)"
    robjects.r(s_command)

    for i_chrom in range(1,24):
        # plot
        s_samplechromosome =  s_sample + ".chromosome" + str(i_chrom)
        ls_sample.append(s_samplechromosome)
        s_opathfile = s_opath + s_samplechromosome + ".pcf.plotgamma.pdf"
        s_command = "plotGamma(df_track, gammaRange=c(" + str(li_gammarange[0]) + "," + str(li_gammarange[1]) +"), dowins=TRUE, sample=1, chrom=" + str(i_chrom) + ", assembly='" + s_assembly + "')"
        robjects.r("pdf('" + s_opathfile + "')")
        robjects.r(s_command)
        robjects.r("dev.off()")

    # out
    return(ls_sample)
