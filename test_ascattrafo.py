import unittest
import os
from myascat.ascattrafo import txtseg2d2t, d2t2txtseg, txtmajorminor2txtseg, trackintersection, d2ttrackdiff, d3ttrack2intersection, d3intersected2txtfasta, txtmajorminor2txtfasta, txtsegdiff

class TestAscat(unittest.TestCase):
    def setUp(self):
        self.s_pathascat = "test_inout/06ascatIn/"
        self.s_ipathigv = "test_inout/txt_igvIn/"
        self.s_opathigv = "test_inout/txt_igvFF/"
        self.s_ipathmedicc = "test_inout/mediccFF_input/"
        if not(os.path.isdir(self.s_ipathmedicc)):
            os.mkdir(self.s_ipathmedicc)
        self.d2t_0k = {
            '2': {12784: (4131224, -0.069494833, None, 'ascattrafoSNP.LogR.PCFed'),
                  16541713: (18257206, -0.067583593, None, 'ascattrafoSNP.LogR.PCFed'),
                  4131266: (16540946, -0.0506811, None, 'ascattrafoSNP.LogR.PCFed'),
                  18257472: (34453722, -0.048106636, None, 'ascattrafoSNP.LogR.PCFed')},
            '16': {51305694: (54137680, -0.033048807, None, 'ascattrafoSNP.LogR.PCFed'),
                   60777: (6404318, -0.077369898, None, 'ascattrafoSNP.LogR.PCFed'),
                   6404837: (35283226, -0.068455793, None, 'ascattrafoSNP.LogR.PCFed'),
                   46463782: (51305207, -0.07158588, None, 'ascattrafoSNP.LogR.PCFed')},
            '4': {10292968: (24036071, -0.050717182, None, 'ascattrafoSNP.LogR.PCFed'),
                  12281: (10291853, -0.088031844, None, 'ascattrafoSNP.LogR.PCFed'),
                  58944570: (72611848, -0.010191058, None, 'ascattrafoSNP.LogR.PCFed'),
                  24038751: (58943676, -0.044868232, None, 'ascattrafoSNP.LogR.PCFed')},
            'X': {2653552: (2703391, 0.0138972, None, 'ascattrafoSNP.LogR.PCFed'),
                  2703633: (3173910, -0.563868584, None, 'ascattrafoSNP.LogR.PCFed'),
                  1999475: (2294639, 0.236431365, None, 'ascattrafoSNP.LogR.PCFed'),
                  3174005: (12498952, -0.589607674, None, 'ascattrafoSNP.LogR.PCFed'),
                  88435254: (90280481, -0.45248459, None, 'ascattrafoSNP.LogR.PCFed'),
                  90280769: (154909159, -0.578127414, None, 'ascattrafoSNP.LogR.PCFed'),
                  39116421: (88433203, -0.572818787, None, 'ascattrafoSNP.LogR.PCFed'),
                  154910730: (155233846, -0.219691516, None, 'ascattrafoSNP.LogR.PCFed'),
                  12498973: (39114059, -0.6031968, None, 'ascattrafoSNP.LogR.PCFed'),
                  168477: (1997200, -0.03905494, None, 'ascattrafoSNP.LogR.PCFed'),
                  2305119: (2645366, -0.06198782, None, 'ascattrafoSNP.LogR.PCFed')},
            '8': {12609361: (20369439, 0.201701094, None, 'ascattrafoSNP.LogR.PCFed'),
                  5372716: (12609117, 0.121826749, None, 'ascattrafoSNP.LogR.PCFed'),
                  1507069: (5372022, 0.182755328, None, 'ascattrafoSNP.LogR.PCFed'),
                  31254: (1506504, 0.103334695, None, 'ascattrafoSNP.LogR.PCFed')}}

        self.d3t_result = {
            'mutant1': {'MT': {20: (30, 1, 'B', 'mutant1'),
                               41: (50, 1, 'B', 'mutant1'),
                               61: (70, 1, 'B', 'mutant1'),
                               100: (110, 2, 'A', 'mutant1'),
                               141: (150, 8, 'D', 'mutant1'),
                               121: (130, 4, 'C', 'mutant1'),
                               171: (180, 16, 'A', 'mutant1')}},
            'mutant2': {'MT': {20: (30, 3, 'B', 'mutant2'),
                               41: (50, 3, 'A', 'mutant2'),
                               61: (70, 3, 'C', 'mutant2'),
                               100: (110, 3, 'D', 'mutant2'),
                               121: (130, 3, 'D', 'mutant2'),
                               141: (150, 3, 'D', 'mutant2'),
                               171: (180, 3, 'A', 'mutant2')}},
            'mutant3': {'MT': {20: (30, 3, 'B', 'mutant3'),
                               41: (50, 3, 'A', 'mutant3'),
                               61: (70, 3, 'C', 'mutant3'),
                               100: (110, 2, 'A', 'mutant3'),
                               121: (130, 4, 'C', 'mutant3'),
                               141: (150, 8, 'D', 'mutant3'),
                               171: (180, 3, 'A', 'mutant3')}}}

        # input
        self.d3t_input = {}
        s_thechromosome = "MT"
        # sample 1
        s_sample = "mutant1"
        d2t_sample = {}
        d2t_sample.update({s_thechromosome : { 20 : ( 70, 1, "B", s_sample)}})
        d2t_sample[s_thechromosome].update({ 90 : ( 110, 2, "A", s_sample)})
        d2t_sample[s_thechromosome].update({ 120 : ( 130, 4, "C", s_sample)})
        d2t_sample[s_thechromosome].update({ 140 : ( 160, 8, "D", s_sample)})
        d2t_sample[s_thechromosome].update({ 170 : ( 180, 16, "A", s_sample)})
        self.d3t_input.update({s_sample : d2t_sample})
        # sample 2
        s_sample = "mutant2"
        d2t_sample = {}
        d2t_sample.update({s_thechromosome : { 10 : ( 30, 3, "B", s_sample)}})
        d2t_sample[s_thechromosome].update({ 40 : ( 50, 3, "A", s_sample)})
        d2t_sample[s_thechromosome].update({ 60 : ( 80, 3, "C", s_sample)})
        d2t_sample[s_thechromosome].update({ 100 : ( 150, 3, "D", s_sample)})
        d2t_sample[s_thechromosome].update({ 170 : ( 180, 3, "A", s_sample)})
        self.d3t_input.update({s_sample : d2t_sample})
        # sample 3
        s_sample = "mutant3"
        d2t_sample = {}
        d2t_sample.update({s_thechromosome : { 11 : ( 31, 3, "B", s_sample)}})
        d2t_sample[s_thechromosome].update({ 41 : ( 51, 3, "A", s_sample)})
        d2t_sample[s_thechromosome].update({ 61 : ( 81, 3, "C", s_sample)})
        d2t_sample[s_thechromosome].update({ 91 : ( 111, 2, "A", s_sample)})
        d2t_sample[s_thechromosome].update({ 121 : ( 131, 4, "C", s_sample)})
        d2t_sample[s_thechromosome].update({ 141 : ( 161, 8, "D", s_sample)})
        d2t_sample[s_thechromosome].update({ 171 : ( 181, 3, "A", s_sample)})
        self.d3t_input.update({s_sample : d2t_sample})


    def test_d2t2txtseg(self):
        d2t2txtseg(d2t_seg=self.d2t_0k, s_opath=self.s_ipathigv)
        s_pathfile = self.s_ipathigv + "ascattrafoSNP.LogR.PCFed.seg"
        d2t_load = txtseg2d2t(s_itxtseg=s_pathfile)
        self.assertEqual(d2t_load, self.d2t_0k)


    def test_txtseg2d2t(self):
        s_itxtseg = self.s_ipathigv + "ascattrafoSNP.LogR.PCFed.seg"
        d2t_load = txtseg2d2t(s_itxtseg)
        self.assertEqual(d2t_load, self.d2t_0k)


    def test_txtmajorminor2txtseg(self):
        s_itxtmajorminor = self.s_pathascat + "ascattrafoSNP.segments.txt"
        s_osegmajor = self.s_opathigv+"ascattrafoSNP.major.seg"
        s_osegminor = self.s_opathigv+"ascattrafoSNP.minor.seg"
        try:
            os.remove(s_osegmajor)
        except:
            pass
        try:
            os.remove(s_osegminor)
        except:
            pass
        txtmajorminor2txtseg(s_itxtmajorminor=s_itxtmajorminor, s_opath=self.s_opathigv)
        b_segmajor = os.path.isfile(s_osegmajor)
        b_segminor = os.path.isfile(s_osegminor)
        self.assertTrue(b_segmajor and b_segminor)


    def test_trackintersection(self):
        s_thechromosome = "MT"
        # the tack
        s_track = "trackintersection"
        d2t_track = {}
        d2t_track.update({s_thechromosome : { 20 : ( 70, 3, "B", s_track)}})
        d2t_track[s_thechromosome].update({ 90 : ( 110, 2, "A", s_track)})
        d2t_track[s_thechromosome].update({ 120 : ( 130, 4, "C", s_track)})
        d2t_track[s_thechromosome].update({ 140 : ( 160, 8, "D", s_track)})
        d2t_track[s_thechromosome].update({ 170 : ( 180, 16, "A", s_track)})
        # the intersection
        dt2_intersection = trackintersection(s_thechromosome=s_thechromosome, i_thebegin=10, i_theend=30, d2t_track=d2t_track)
        self.assertEqual(dt2_intersection, {'MT': {20: (30, 3, 'B', 'trackintersection')}})
        dt2_intersection = trackintersection(s_thechromosome=s_thechromosome, i_thebegin=40, i_theend=50, d2t_track=d2t_track)
        self.assertEqual(dt2_intersection, {'MT': {40: (50, 3, 'B', 'trackintersection')}})
        dt2_intersection = trackintersection(s_thechromosome=s_thechromosome, i_thebegin=60, i_theend=80, d2t_track=d2t_track)
        self.assertEqual(dt2_intersection, {'MT': {60: (70, 3, 'B', 'trackintersection')}})
        dt2_intersection = trackintersection(s_thechromosome=s_thechromosome, i_thebegin=100, i_theend=150, d2t_track=d2t_track)
        self.assertEqual(dt2_intersection, {'MT': {120: (130, 4, 'C', 'trackintersection'), 140: (150, 8, 'D', 'trackintersection'), 100: (110, 2, 'A', 'trackintersection')}})
        dt2_intersection = trackintersection(s_thechromosome=s_thechromosome, i_thebegin=170, i_theend=180, d2t_track=d2t_track)
        self.assertEqual(dt2_intersection, {'MT': {170: (180, 16, 'A', 'trackintersection')}})


    def test_d2ttrackdiff(self):
        d2t_result = {
            'MT': {20: (30, -2, 'B', 'difftrack_mutant-wildtype'),
                   40: (50, -2, 'B', 'difftrack_mutant-wildtype'),
                   60: (70, -2, 'B', 'difftrack_mutant-wildtype'),
                   100: (110, -1, 'A', 'difftrack_mutant-wildtype'),
                   120: (130, 1, 'C', 'difftrack_mutant-wildtype'),
                   140: (150, 5, 'D', 'difftrack_mutant-wildtype'),
                   170: (180, 13, 'A', 'difftrack_mutant-wildtype')}}

        # mt track
        s_thechromosome = "MT"
        s_track = "mutant"
        d2t_mt = {}
        d2t_mt.update({s_thechromosome : { 20 : ( 70, 1, "B", s_track)}})
        d2t_mt[s_thechromosome].update({ 90 : ( 110, 2, "A", s_track)})
        d2t_mt[s_thechromosome].update({ 120 : ( 130, 4, "C", s_track)})
        d2t_mt[s_thechromosome].update({ 140 : ( 160, 8, "D", s_track)})
        d2t_mt[s_thechromosome].update({ 170 : ( 180, 16, "A", s_track)})
        # wt track
        s_track = "wildtype"
        d2t_wt = {}
        d2t_wt.update({s_thechromosome : { 10 : ( 30, 3, "B", s_track)}})
        d2t_wt[s_thechromosome].update({ 40 : ( 50, 3, "A", s_track)})
        d2t_wt[s_thechromosome].update({ 60 : ( 80, 3, "C", s_track)})
        d2t_wt[s_thechromosome].update({ 100 : ( 150, 3, "D", s_track)})
        d2t_wt[s_thechromosome].update({ 170 : ( 180, 3, "A", s_track)})
        # process
        s_track = None
        d2t_diff = d2ttrackdiff(d2t_mt=d2t_mt, d2t_wt=d2t_wt, s_track=s_track)
        self.assertEqual(d2t_diff, d2t_result)


    def test_d3ttrack2intersection(self):
        # process
        d3t_out = d3ttrack2intersection(d3t_track=self.d3t_input)
        self.assertEqual(d3t_out, self.d3t_result)


    def test_txtsegdiff(self):
        s_itxtsegmt = self.s_ipathigv+"mutant_SHELF_H11_SNP6.LogR.PCFed.seg"
        s_itxtsegwt = self.s_ipathigv+"mutant_SHELF_H12_SNP6.LogR.PCFed.seg"
        s_ofilen = txtsegdiff(s_itxtsegmt=s_itxtsegmt, s_itxtsegwt=s_itxtsegwt, s_opath=self.s_opathigv)
        self.assertEqual(s_ofilen, self.s_opathigv + "difftrack_mutant_SHELF_H11_SNP6.LogR.PCFed-mutant_SHELF_H12_SNP6.LogR.PCFed.seg")


    def test_d3intersected2txtfasta(self):
        s_otxtprefix = "unittest"
        s_maijnor = "major"
        ls_descr = d3intersected2txtfasta(d3t_intersected=self.d3t_result, s_maijnor=s_maijnor, s_otxtprefix=s_otxtprefix, s_opath=self.s_ipathmedicc)
        self.assertEqual(ls_descr, [s_otxtprefix + "_MT_" + s_maijnor + ".fasta"])


    def test_txtmajorminor2txtfasta(self):
        b_0k = False
        s_otxtprefix = "unittest"
        ls_itxtmajorminor = [
            self.s_pathascat+"mutant_SHELF_H08_SNP6.segments.txt",
            self.s_pathascat+"mutant_SHELF_H09_SNP6.segments.txt",
            self.s_pathascat+"mutant_SHELF_H11_SNP6.segments.txt",
            self.s_pathascat+"mutant_SHELF_H12_SNP6.segments.txt"]
        b_0k = txtmajorminor2txtfasta(ls_itxtmajorminor=ls_itxtmajorminor, s_otxtprefix=s_otxtprefix, s_opathigv=self.s_opathigv, s_opathmedicc=self.s_ipathmedicc)
        self.assertTrue(b_0k)


if __name__ == '__main__':
    unittest.main(warnings='ignore')
