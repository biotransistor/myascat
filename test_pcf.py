import unittest
from myascat.pcf import plotallele, plotgamma

class TestAscat(unittest.TestCase):
    def setUp(self):
        self.s_ipath = "test_inout/05lrrbaf2logrbafIn/"
        self.s_pathmain = "test_inout/"
        self.s_logr = "pcftrack_LogR.txt"
        self.s_baf = "pcftrack_BAF.txt"
        self.b_verbose = False

    def test_plotallele(self):
        ls_result = ['pcftrack_LogR']
        ls_sample = plotallele(s_logr=self.s_logr, s_baf=self.s_baf, s_pathmain=self.s_pathmain, s_ipath= self.s_ipath, b_verbose=self.b_verbose)
        self.assertEqual(ls_sample, ls_result)

    def test_plotgamma(self):
        ls_result = ['pcftrack_LogR.chromosome1', 'pcftrack_LogR.chromosome2', 'pcftrack_LogR.chromosome3', 'pcftrack_LogR.chromosome4', 'pcftrack_LogR.chromosome5', 'pcftrack_LogR.chromosome6', 'pcftrack_LogR.chromosome7', 'pcftrack_LogR.chromosome8', 'pcftrack_LogR.chromosome9', 'pcftrack_LogR.chromosome10', 'pcftrack_LogR.chromosome11', 'pcftrack_LogR.chromosome12', 'pcftrack_LogR.chromosome13', 'pcftrack_LogR.chromosome14', 'pcftrack_LogR.chromosome15', 'pcftrack_LogR.chromosome16', 'pcftrack_LogR.chromosome17', 'pcftrack_LogR.chromosome18', 'pcftrack_LogR.chromosome19', 'pcftrack_LogR.chromosome20', 'pcftrack_LogR.chromosome21', 'pcftrack_LogR.chromosome22', 'pcftrack_LogR.chromosome23']
        ls_sample = plotgamma(s_trackfile=self.s_logr, s_pathmain=self.s_pathmain, s_ipath=self.s_ipath)
        self.assertEqual(ls_sample, ls_result)


if __name__ == '__main__':
        unittest.main(warnings='ignore')
